webpackJsonp([3],{

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageModule", function() { return SignupPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__signup__ = __webpack_require__(329);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SignupPageModule = (function () {
    function SignupPageModule() {
    }
    return SignupPageModule;
}());
SignupPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__signup__["a" /* SignupPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__signup__["a" /* SignupPage */]),
            __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["b" /* TranslateModule */].forChild()
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_3__signup__["a" /* SignupPage */]
        ]
    })
], SignupPageModule);

//# sourceMappingURL=signup.module.js.map

/***/ }),

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_providers__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages__ = __webpack_require__(217);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SignupPage = (function () {
    function SignupPage(navCtrl, user, navParams, loadingCtrl, alertCtrl, toastCtrl, translateService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.user = user;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.translateService = translateService;
        this.User = false;
        this.creating_account = false;
        this.account = {
            names: '',
            email: '',
            password: ''
        };
        this.User = navParams.get("User") ? navParams.get("User") : this.User;
        this.translateService.get('SIGNUP_ERROR').subscribe(function (value) {
            _this.signupErrorString = value;
        });
        console.log(this.User);
    }
    SignupPage.prototype.presentLoading = function () {
        this.creating_account = this.loadingCtrl.create({
            content: "Creando tu cuenta",
            duration: 8000
        });
        this.creating_account.present();
    };
    SignupPage.prototype.checkData = function (email, password, names) {
        if (!email) {
            var alert = this.alertCtrl.create({
                title: 'Ops!',
                subTitle: 'No has introducido tu correo electrónico, intenta otra vez.',
                buttons: ['OK']
            });
            alert.present();
            return false;
        }
        if (!password) {
            var alert = this.alertCtrl.create({
                title: 'Ops!',
                subTitle: 'No has introducido una contraseña válida, intenta otra vez.',
                buttons: ['OK']
            });
            alert.present();
            return false;
        }
        if (!names) {
            var alert = this.alertCtrl.create({
                title: 'Ops!',
                subTitle: 'No has introducido un nombre válido, intenta otra vez.',
                buttons: ['OK']
            });
            alert.present();
            return false;
        }
        return true;
    };
    SignupPage.prototype.setUser = function (response, welcomePush) {
        var _this = this;
        this.User.setUser(response.data, function () {
            _this.User.setUser(response.data, function () {
                _this.User.saveRegistration(welcomePush, function (response) {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages__["b" /* MainPage */]);
                });
            });
        });
    };
    SignupPage.prototype.doLogin = function (email, password, company_id) {
        var _this = this;
        if (this.checkData(this.account.email, this.account.password, this.account.names)) {
            var loader_1 = this.loadingCtrl.create({
                content: "Iniciando sesión...",
            });
            loader_1.present();
            this.User.loginUser(function (response) {
                loader_1.dismiss();
                if (response.s == 1) {
                    _this.setUser(response, false);
                }
                else if (response.s == 0) {
                    if (response.r == 'CONFIRM_ACCOUNT_CREATION') {
                        var alert = _this.alertCtrl.create({
                            title: 'Ops!',
                            subTitle: 'El usuario y la contraseña no coinciden. ¿Deseas crear un usuario con el correo <b>' + email + '</b>?',
                            buttons: [
                                {
                                    text: 'Aceptar',
                                    handler: function (data) {
                                        _this.doSignup();
                                    }
                                },
                                {
                                    text: 'Cancelar',
                                    handler: function (data) {
                                    }
                                }
                            ]
                        });
                        alert.present();
                    }
                    else if (response.r == 'NOT_LOADED') {
                        var alert = _this.alertCtrl.create({
                            title: 'Error',
                            subTitle: 'El correo electrónico o la contraseña no coinciden',
                            buttons: [
                                {
                                    text: 'Aceptar',
                                    handler: function (data) {
                                    }
                                }
                            ]
                        });
                        alert.present();
                    }
                }
            }, { company_id: company_id, mail: email, password: password, name: this.account.name });
        }
    };
    SignupPage.prototype.doSignup = function () {
        var _this = this;
        if (this.checkData(this.account.email, this.account.password, this.account.names)) {
            this.presentLoading();
            this.User.singUpNewUser(function (response) {
                if (response.s == 1) {
                    _this.creating_account.dismiss();
                    _this.setUser(response, true);
                }
                else if (response.r == "MAIL_ALREADY_EXIST") {
                    var alert = _this.alertCtrl.create({
                        title: 'Ops!',
                        subTitle: 'El correo electrónico introducido ya existe. ¿Deseas ingresar?',
                        buttons: [
                            {
                                text: 'Ingresar a mi Cuenta',
                                handler: function (data) {
                                    _this.creating_account.dismiss();
                                    _this.doLogin(_this.account.email, _this.account.password, _this.account.names);
                                }
                            },
                            {
                                text: 'Cancelar',
                                handler: function (data) {
                                    _this.creating_account.dismiss();
                                }
                            }
                        ]
                    });
                    alert.present();
                }
            }, { mail: this.account.email, password: this.account.password, names: this.account.names });
        }
    };
    return SignupPage;
}());
SignupPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-signup',template:/*ion-inline-start:"/Applications/MAMP/htdocs/cancer/src/pages/signup/signup.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>{{ \'SIGNUP_TITLE\' | translate }}</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <form (submit)="doSignup()">\n    <ion-list>\n\n      <ion-item>\n        <ion-label fixed>{{ \'NAME\' | translate }}</ion-label>\n        <ion-input type="text" [(ngModel)]="account.names" name="names"></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label fixed>{{ \'EMAIL\' | translate }}</ion-label>\n        <ion-input type="email" [(ngModel)]="account.email" name="email"></ion-input>\n      </ion-item>\n\n      <!--\n      Want to add a Username? Here you go:\n\n      <ion-item>\n        <ion-label floating>Username</ion-label>\n        <ion-input type="text" [(ngModel)]="account.username" name="username"></ion-input>\n      </ion-item>\n      -->\n\n      <ion-item>\n        <ion-label fixed>{{ \'PASSWORD\' | translate }}</ion-label>\n        <ion-input type="password" [(ngModel)]="account.password" name="password"></ion-input>\n      </ion-item>\n\n      <div padding>\n        <button ion-button color="primary" block>{{ \'SIGNUP_BUTTON\' | translate }}</button>\n      </div>\n\n    </ion-list>\n  </form>\n</ion-content>'/*ion-inline-end:"/Applications/MAMP/htdocs/cancer/src/pages/signup/signup.html"*/
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__providers_providers__["d" /* User */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_providers__["d" /* User */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* ToastController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* ToastController */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["c" /* TranslateService */]) === "function" && _g || Object])
], SignupPage);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=signup.js.map

/***/ })

});
//# sourceMappingURL=3.js.map