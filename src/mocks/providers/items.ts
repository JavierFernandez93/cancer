import { Injectable } from '@angular/core';

import { Item } from '../../models/item';

@Injectable()
export class Items {
  itemsDone: any = false;
  percentaje: any = 0;
  items: Item[] = [];
  defaultItem: any = {
    "name": "5 Plátanos para hoy",
    "profilePic": "https://estaticos.muyinteresante.es/media/cache/1000x460_thumb/uploads/images/gallery/579f2df65bafe8178b8b456d/platano2.jpg",
    "about": "Esté alimento proporciona potasio.",
    "done": false
  };

  constructor() {
    let items = [
      {
        "name": "Día 1 - 1 nota guardada",
        "profilePic": "https://image.flaticon.com/icons/svg/124/124032.svg",
        "about": "Revisión correcta",
        "done": false
      },
      {
        "name": "Día 2 - Ninguna nota",
        "profilePic": "https://image.flaticon.com/icons/svg/124/124032.svg",
        "about": "Revisión correcta",
        "done": false
      },
      {
        "name": "Día 3 - 2 notas guardadas",
        "profilePic": "https://image.flaticon.com/icons/svg/124/124032.svg",
        "about": "Revisión correcta",
        "done": false
      },
      {
        "name": "Día 4 - Ninguna nota",
        "profilePic": "https://image.flaticon.com/icons/svg/124/124032.svg",
        "about": "Revisión correcta",
        "done": false
      },
      {
        "name": "Día 5 - Ninguna nota",
        "profilePic": "https://image.flaticon.com/icons/svg/124/124032.svg",
        "about": "Revisión correcta",
        "done": false
      },

      {
        "name": "Día 6 - Ninguna nota",
        "profilePic": "https://image.flaticon.com/icons/svg/124/124032.svg",
        "about": "Revisión correcta",
        "done": false
      },
      {
        "name": "Día 7 - Ninguna nota",
        "profilePic": "https://image.flaticon.com/icons/svg/124/124032.svg",
        "about": "Revisión correcta",
        "done": false
      },
      {
        "name": "Día 8 - Ninguna nota",
        "profilePic": "https://image.flaticon.com/icons/svg/124/124032.svg",
        "about": "Revisión correcta",
        "done": false
      },
      {
        "name": "Día 9 - 1 nota guardada",
        "profilePic": "https://image.flaticon.com/icons/svg/124/124032.svg",
        "about": "Revisión correcta",
        "done": false
      },
      
      {
        "name": "Día 10 - Ninguna nota",
        "profilePic": "https://image.flaticon.com/icons/svg/124/124032.svg",
        "about": "Revisión correcta",
        "done": false
      },
      {
        "name": "Día 11 - Ninguna nota",
        "profilePic": "https://image.flaticon.com/icons/svg/124/124032.svg",
        "about": "Revisión correcta",
        "done": false
      },
      {
        "name": "Día 12 - Ninguna nota",
        "profilePic": "https://image.flaticon.com/icons/svg/124/124032.svg",
        "about": "Revisión correcta",
        "done": false
      },
      {
        "name": "Día 13 - Ninguna nota",
        "profilePic": "https://image.flaticon.com/icons/svg/124/124032.svg",
        "about": "Revisión correcta",
        "done": false
      },
    ];

    for (let item of items) {
      this.items.push(new Item(item));
    }
    this.getPercentaje();
  }

  query(params?: any) {
    if (!params) {
      return this.items;
    }

    return this.items.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  countDone() {
    let done = this.items.filter((item) => {
        return item['done'] ? item : false;
    });
    this.itemsDone = done.length;
  }
  getPercentaje() {
    this.countDone();
    
    this.percentaje = (this.itemsDone * 100) / this.items.length;
  }
  add(item: Item) {
    this.items.push(item);
  }

  isDone() {
    let done = this.items.filter((item) => {
        return item['done'] ? item : false;
    });
    if(done.length == this.items.length)
      return true;

    return false;
  }

  delete(item: Item) {
    this.items.splice(this.items.indexOf(item), 1);
  }

  done(item: Item) {
    this.items[this.items.indexOf(item)]['done'] = true;
  }
}
