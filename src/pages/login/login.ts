import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, AlertController, ToastController } from 'ionic-angular';

import { User } from '../../providers/providers';
import { MainPage } from '../pages';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { email: string, password: string } = {
    email: 'javier.fernandez.pa93@gmail.com',
    password: 'MiContraseña'
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    private alertCtrl:AlertController,
    public toastCtrl: ToastController,
    public translateService: TranslateService) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  }

  checkData(email,password){
    if(!email)
    {
      let alert = this.alertCtrl.create({
        title: 'Ops!',
        subTitle: 'No has introducido tu correo electrónico, intenta otra vez.',
        buttons: ['OK']
      });

      alert.present();
      return false;
    }

    if(!password)
    {
      let alert = this.alertCtrl.create({
        title: 'Ops!',
        subTitle: 'No has introducido una contraseña válida, intenta otra vez.',
        buttons: ['OK']
      });
      alert.present();
      return false;
    }

    return true;
  }

  doLogin(email,password,company_id) {
    if(this.checkData(email,password))
    {
      let loader = this.loadingCtrl.create({
        content: "Iniciando sesión...",
      });

      loader.present();

      this.getPosition((response)=>{
        this.User.loginUser((response)=>{
          loader.dismiss();
          if(response.s == 1)
          {
            let has_pending_order = false;
            if(response.data.user_settings.kind == 0)
            {
              if(response.has_pending_order)
                has_pending_order = true;
            }
            this.setUser(response,false);
          } else if(response.s == 0){
            if(response.r == 'CONFIRM_ACCOUNT_CREATION')
            {
              let alert = this.alertCtrl.create({
                title: 'Ops!',
                subTitle: 'El usuario y la contraseña no coinciden. ¿Deseas crear un usuario con el correo <b>'+email+'</b>?',
                buttons: [
                  {
                    text: 'Aceptar',
                    handler: data => {
                      this.makeAccount(email,password);
                    }
                  },
                  {
                    text: 'Cancelar',
                    handler: data => {
                    }
                  }
                ]
              });
              alert.present();
            } else if(response.r == 'ACCOUNTS_FOUNDED') {
              this.chosseAccount(response.accounts);
            } else if(response.r == 'NOT_LOADED') {
               let alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'El correo electrónico o la contraseña no coinciden',
                buttons: [
                  {
                    text: 'Aceptar',
                    handler: data => {
                    }
                  }
                ]
              });
              alert.present();
            }
          }
        },{company_id:company_id,mail:email,password:password,latitude:this.latitude,longitude:this.longitude});
      })
    }
  }

  // Attempt to login in through our User service
  /*doLogin() {
    // this.user.login(this.account).subscribe((resp) => {
      this.navCtrl.push(MainPage);
    // }, (err) => {
    //   this.navCtrl.push(MainPage);
    //   // Unable to log in
    //   let toast = this.toastCtrl.create({
    //     message: this.loginErrorString,
    //     duration: 3000,
    //     position: 'top'
    //   });
    //   toast.present();
    // });
  }*/
}
