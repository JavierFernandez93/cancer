import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, LoadingController, AlertController, NavParams, NavController, ToastController } from 'ionic-angular';

import { User } from '../../providers/providers';
import { MainPage } from '../pages';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  User:any = false;
  creating_account:any = false;
  account: { names: string, email: string, password: string } = {
    names: '',
    email: '',
    password: ''
  };
  private signupErrorString: string;
  constructor(public navCtrl: NavController,
    public user: User,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public translateService: TranslateService) {
    this.User = navParams.get("User") ? navParams.get("User") : this.User;
    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    });

    console.log(this.User);
  }
  presentLoading(){
    this.creating_account = this.loadingCtrl.create({
      content: "Creando tu cuenta",
      duration: 8000
    });
    this.creating_account.present();
  }
  checkData(email,password,names){
    if(!email)
    {
      let alert = this.alertCtrl.create({
        title: 'Ops!',
        subTitle: 'No has introducido tu correo electrónico, intenta otra vez.',
        buttons: ['OK']
      });

      alert.present();
      return false;
    }

    if(!password) {
      let alert = this.alertCtrl.create({
        title: 'Ops!',
        subTitle: 'No has introducido una contraseña válida, intenta otra vez.',
        buttons: ['OK']
      });
      alert.present();
      return false;
    }

    if(!names) {
      let alert = this.alertCtrl.create({
        title: 'Ops!',
        subTitle: 'No has introducido un nombre válido, intenta otra vez.',
        buttons: ['OK']
      });
      alert.present();
      return false;
    }

    return true;
  }
  setUser(response,welcomePush){
    this.User.setUser(response.data,()=>{
      this.User.setUser(response.data,()=>{
        this.User.saveRegistration(welcomePush,(response)=>{
          this.navCtrl.push(MainPage);
        });
      });
    });
  }
  doLogin(email,password,company_id) {
    if(this.checkData(this.account.email,this.account.password,this.account.names))
    {
      let loader = this.loadingCtrl.create({
        content: "Iniciando sesión...",
      });

      loader.present();

      this.User.loginUser((response)=>{
        loader.dismiss();
        if(response.s == 1) {
          this.setUser(response,false);
        } else if(response.s == 0){
          if(response.r == 'CONFIRM_ACCOUNT_CREATION')
          {
            let alert = this.alertCtrl.create({
              title: 'Ops!',
              subTitle: 'El usuario y la contraseña no coinciden. ¿Deseas crear un usuario con el correo <b>'+email+'</b>?',
              buttons: [
                {
                  text: 'Aceptar',
                  handler: data => {
                    this.doSignup();
                  }
                },
                {
                  text: 'Cancelar',
                  handler: data => {
                  }
                }
              ]
            });
            alert.present();
          } else if(response.r == 'NOT_LOADED') {
             let alert = this.alertCtrl.create({
              title: 'Error',
              subTitle: 'El correo electrónico o la contraseña no coinciden',
              buttons: [
                {
                  text: 'Aceptar',
                  handler: data => {
                  }
                }
              ]
            });
            alert.present();
          }
        }
      },{company_id:company_id,mail:email,password:password,name:this.account.name});
    }
  }
  doSignup() {
    if(this.checkData(this.account.email,this.account.password,this.account.names))
    {
      this.presentLoading();
      this.User.singUpNewUser((response)=>{
        if(response.s == 1)
        {
          this.creating_account.dismiss();
          this.setUser(response,true);
        } else if(response.r == "MAIL_ALREADY_EXIST"){
          let alert = this.alertCtrl.create({
            title: 'Ops!',
            subTitle: 'El correo electrónico introducido ya existe. ¿Deseas ingresar?',
            buttons: [
              {
                text: 'Ingresar a mi Cuenta',
                handler: data => {
                  this.creating_account.dismiss();
                  this.doLogin(this.account.email,this.account.password,this.account.names);
                }
              },
              {
                text: 'Cancelar',
                handler: data => {
                  this.creating_account.dismiss();
                }
              }
            ]
          });
          alert.present();
        }
      },{mail:this.account.email,password:this.account.password,names:this.account.names});
    }
  }
}
