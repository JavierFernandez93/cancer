import { Component } from '@angular/core';
import { IonicPage, NavParams, NavController } from 'ionic-angular';

/**
 * The Welcome Page is a splash page that quickly describes the app,
 * and then directs the user to create an account or log in.
 * If you'd like to immediately put the user onto a login/signup page,
 * we recommend not using the Welcome page.
*/
@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})
export class WelcomePage {
	User:any = false;
  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams) { 
    this.User = navParams.get("User") ? navParams.get("User") : this.User;
    console.log(this.User);
  }

  login() {
    this.navCtrl.push('LoginPage',{User:this.User});
  }

  signup() {
    this.navCtrl.push('SignupPage',{User:this.User});
  }
}
