import { Component } from '@angular/core';
import { IonicPage, MenuController, NavController, Platform } from 'ionic-angular';

import { User } from '../../providers/providers';
import { TranslateService } from '@ngx-translate/core';
import { MainPage } from '../pages';
import { OneSignal } from '@ionic-native/onesignal';

export interface Slide {
  title: string;
  description: string;
  image: string;
}

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html'
})
export class TutorialPage {
  slides: Slide[];
  showSkip = true;
  dir: string = 'ltr';

  constructor(
    public navCtrl: NavController, 
    private oneSignal: OneSignal,
    public user: User,
    public menu: MenuController, 
    public translate: TranslateService, 
    public platform: Platform) {
    this.dir = platform.dir();
    this.iniPush((response)=>{
      this.user.getUser((response)=>{
        if(this.user.logged && response) {
          this.user.getUser((response)=>{
            console.log(this.user);
            this.navCtrl.setRoot(MainPage,{User:this.user});
          });
        } else {
          this.initTranslation();
        }
      });
    });
  }

  initTranslation(){
    this.translate.get(["TUTORIAL_SLIDE1_TITLE",
      "TUTORIAL_SLIDE1_DESCRIPTION",
      "TUTORIAL_SLIDE2_TITLE",
      "TUTORIAL_SLIDE2_DESCRIPTION",
      "TUTORIAL_SLIDE3_TITLE",
      "TUTORIAL_SLIDE3_DESCRIPTION",
    ]).subscribe((values) => {
      console.log('Loaded values', values);
      this.slides = [
        {
          title: values.TUTORIAL_SLIDE1_TITLE,
          description: values.TUTORIAL_SLIDE1_DESCRIPTION,
          image: 'assets/img/ribbon-256.png',
        },
        {
          title: values.TUTORIAL_SLIDE2_TITLE,
          description: values.TUTORIAL_SLIDE2_DESCRIPTION,
          image: 'assets/img/smartphone-256.png',
        },
        {
          title: values.TUTORIAL_SLIDE3_TITLE,
          description: values.TUTORIAL_SLIDE3_DESCRIPTION,
          image: 'assets/img/patient-256.png',
        }
      ];
    });
  }

  iniPush(callback:any){
    this.platform.ready().then(() => {
      if(this.platform.is('android') || this.platform.is('ios'))
      {
        this.oneSignal.startInit("83dc28f8-5215-4a31-bf3c-b2b254a85904", "707425170960");
        // OneSignal.inFocusDisplaying(OneSignal.OSInFocusDisplayOption.InAppAlert);
        this.oneSignal.setSubscription(true);
        this.oneSignal.handleNotificationReceived().subscribe(() => {
        // do something when the notification is received.
        });
        this.oneSignal.handleNotificationOpened().subscribe((response) => {
          let oresponse = response;
          if(response.notification.payload.additionalData.loadSpecialData)
          {
            this.user.getSpecialData((response)=>{
              if(response.s == 1)
              {
                this.goToAction(oresponse,response.data);
              }
            },response.notification.payload.additionalData);
          } else {
            this.goToAction(oresponse,response.notification.payload.additionalData.additionalInfo);
          }
        });

        this.oneSignal.endInit();
        callback();
      } 
    });

    callback();
  }

  goToAction = (response:any,data:any) : any => {
    switch (response.action.actionID) {
      case "acept_welcome":
        this.navCtrl.push('Foodhomeuser',{User:this.user})
        // code...
        break;
      
      default:
        // code...
        break;
    }
  }

  startApp() {
    this.navCtrl.setRoot('WelcomePage', {User:this.user}, {
      animate: true,
      direction: 'forward'
    });
  }

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd();
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }

}
