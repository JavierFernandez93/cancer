var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Routes } from '../routes/routes';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { OneSignal } from '@ionic-native/onesignal';
import { Platform, AlertController, LoadingController } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import { BackgroundMode } from '@ionic-native/background-mode';
import { Geolocation } from '@ionic-native/geolocation';
import { Storage } from '@ionic/storage';
/*
  Generated class for the UserProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var User = /** @class */ (function () {
    function User(loadingCtrl, backgroundMode, geolocation, _photoViewer, backgroundGeolocation, storage, platform, alertCtrl, diagnostic, oneSignal, http, routes) {
        var _this = this;
        this.loadingCtrl = loadingCtrl;
        this.backgroundMode = backgroundMode;
        this.geolocation = geolocation;
        this._photoViewer = _photoViewer;
        this.backgroundGeolocation = backgroundGeolocation;
        this.storage = storage;
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.diagnostic = diagnostic;
        this.oneSignal = oneSignal;
        this.http = http;
        this.routes = routes;
        this.x = false;
        this.OneSignal = false;
        this.oneSignalProyectId = false;
        this.googleAppId = false;
        this.logged = false;
        this.registration_id = false;
        this.userObject = false;
        /* fast vars */
        this.is_able_to_notification = false;
        this.is_able_to_tracking = false;
        this.tracking_time = 10000;
        this.subscription = false;
        this.birthdate = false;
        this.short_name = false;
        this.commerces_near = false;
        this.gold_coin = false;
        this.range = false;
        this.commerce_status = 0;
        this.reward = 0;
        this.wallet = 0;
        this.kind = false;
        this.silver_coin = false;
        this.image = false;
        this.commerce_id = false;
        this.names = false;
        this.mail = false;
        this.responseOneSignal = false;
        this.isLogged = function () {
            _this.getUser(function () { });
            return _this.logged;
        };
        this.getAllForMap = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_commerces_for_map.php' }, callback, data);
        };
        this.setAmountOfCommerces = function (commerces_near, callback) {
            _this.storage.get("userObject").then(function (data) {
                _this.commerces_near = commerces_near;
                data.commerces_near = commerces_near;
                _this.setUser(data, function (reponse) {
                    callback(true);
                });
            });
        };
        this.getAccountCreated = function (callback) {
            _this.storage.get("account_created").then(function (data) {
                if (data != null) {
                    if (callback != undefined)
                        callback(true);
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.setTutoAsViewed = function (callback, tuto) {
            _this.storage.set(tuto, true).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.getTutoAsViewed = function (callback, tuto) {
            _this.storage.get(tuto).then(function (data) {
                if (data != null) {
                    if (callback != undefined)
                        callback(data);
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.getAllPromoMine = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_my_commerces.php' }, callback, data);
        };
        this.setCommerceStatus = function (callback, status) {
            _this.storage.set("commerce_active", status).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.getCommerceStatus = function (callback) {
            _this.storage.get("commerce_active").then(function (data) {
                if (data != null) {
                    if (callback != undefined)
                        callback(data);
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.setAccountCreated = function (callback) {
            _this.storage.set("account_created", true).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.getAccountEmail = function (callback) {
            _this.storage.get("email").then(function (data) {
                if (data != null) {
                    if (callback != undefined)
                        callback(data);
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.setSpecialDish = function (dish, callback) {
            _this.getSpecialDishes(function (response) {
                var dishes = [];
                if (response) {
                    dishes = response;
                }
                dishes.push(dish);
                _this.storage.set("dishes", dishes).then(function () {
                    if (callback != undefined)
                        callback(true);
                });
            });
        };
        this.getSpecialDishes = function (callback) {
            _this.storage.get("dishes").then(function (data) {
                if (data != null) {
                    if (callback != undefined)
                        callback(data);
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.setDishOrderPerUser = function (dish_order_per_user, callback) {
            _this.storage.set("dish_order_per_user", dish_order_per_user).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.getDishOrderPerUser = function (callback) {
            _this.storage.get("dish_order_per_user").then(function (data) {
                if (data != null) {
                    if (callback != undefined)
                        callback(data);
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.setOrderPerUserId = function (order_per_user_id, callback) {
            _this.storage.set("order_per_user_id", order_per_user_id).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.getOrderPerUserId = function (callback) {
            _this.storage.get("order_per_user_id").then(function (data) {
                if (data != null) {
                    if (callback != undefined)
                        callback(data);
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.setPrewelcome = function (callback) {
            _this.storage.set("prewelcome", true).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.deleteOrderPerUserId = function (callback) {
            _this.storage.set("order_per_user_id", false).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.deletePrewelcome = function (callback) {
            _this.storage.set("prewelcome", false).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.getPrewelcome = function (callback) {
            _this.storage.get("prewelcome").then(function (data) {
                if (data != null) {
                    if (callback != undefined)
                        callback(data);
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.setAccountEmail = function (email, callback) {
            _this.storage.set("email", email).then(function () {
                if (callback != undefined)
                    callback(true);
            });
        };
        this.setUser = function (data, callback) {
            _this.storage.set("userObject", data).then(function () {
                _this.setAccountCreated(function (response) {
                    _this.setAccountEmail(data.user_login.mail, function (response) {
                        _this.userObject = data;
                        _this.logged = true;
                        _this.setDefaults(function (response) {
                            if (callback != undefined)
                                callback(_this.userObject);
                        });
                    });
                });
            });
        };
        this.setUserInBoardFromPrewelcome = function (callback, data) {
            return _this.routes.getProvider({ url: 'set_user_in_a_board_from_prewelcome.php' }, callback, data);
        };
        this.acceptCommerceAuction = function (callback, data) {
            return _this.routes.getProvider({ url: 'accept_auction_per_commerce.php' }, callback, data);
        };
        this.getPaymentLog = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_payment_log.php' }, callback, data);
        };
        this.loginWithFacebook = function (callback, data) {
            return _this.routes.getProvider({ url: 'login_user_with_facebook.php' }, callback, data);
        };
        this.loginUser = function (callback, data) {
            return _this.routes.getProvider({ url: 'login_user.php' }, callback, data);
        };
        this.updateUser = function (callback, data) {
            return _this.routes.getProvider({ url: 'update_user.php' }, callback, data);
        };
        this.updateUserPhoto = function (callback, data) {
            return _this.routes.getProvider({ url: 'update_user_photo.php' }, callback, data);
        };
        this.deleteDish = function (callback, data) {
            return _this.routes.getProvider({ url: 'delete_dish_user.php' }, callback, data);
        };
        this.saveBuyForReload = function (callback, data) {
            return _this.routes.getProvider({ url: 'save_buy_for_reload.php' }, callback, data);
        };
        this.getCommercesForSearch = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_commerces_for_search.php' }, callback, data);
        };
        this.getAllMyFabs = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_my_fabs.php' }, callback, data);
        };
        this.getAllForSearch = function (callback, data) {
            return _this.routes.getProvider({ url: 'make_fab.php' }, callback, data);
        };
        this.makeFab = function (callback, data) {
            return _this.routes.getProvider({ url: 'make_fab.php' }, callback, data);
        };
        this.getAllMyHistory = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_my_history.php' }, callback, data);
        };
        this.registratePayPalBuy = function (callback, data) {
            return _this.routes.getProvider({ url: 'registrate_paypal_buy.php' }, callback, data);
        };
        this.saveBuy = function (callback, data) {
            return _this.routes.getProvider({ url: 'save_buy.php' }, callback, data);
        };
        this.makeUserAuction = function (callback, data) {
            return _this.routes.getProvider({ url: 'make_auction_user.php' }, callback, data);
        };
        this.deleteAuction = function (callback, data) {
            return _this.routes.getProvider({ url: 'delete_auction.php' }, callback, data);
        };
        this.callToWaiter = function (callback, data) {
            return _this.routes.getProvider({ url: 'call_to_waiter.php' }, callback, data);
        };
        this.getReload = function (callback, data) {
            return _this.routes.getProvider({ url: 'regenerate_saved_reload.php' }, callback, data);
        };
        this.getBuy = function (callback, data) {
            return _this.routes.getProvider({ url: 'regenerate_saved_buy.php' }, callback, data);
        };
        this.getAllUserAuctions = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_auction_user.php' }, callback, data);
        };
        this.updateReamingTime = function (callback, data) {
            return _this.routes.getProvider({ url: 'update_reaming_time.php' }, callback, data);
        };
        this.getCatalogActivity = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_catalog_activiy.php' }, callback, data);
        };
        this.getAllUserActiveAuctions = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_auction_user_active.php' }, callback, data);
        };
        this.resetOrder = function (callback, data) {
            return _this.routes.getProvider({ url: 'reset_order.php' }, callback, data);
        };
        this.checkIfAuctionIsTaked = function (callback, data) {
            return _this.routes.getProvider({ url: 'check_if_auction_is_taked.php' }, callback, data);
        };
        this.acceptAuction = function (callback, data) {
            return _this.routes.getProvider({ url: 'accept_auction.php' }, callback, data);
        };
        this.setAuctionAsExpired = function (callback, data) {
            return _this.routes.getProvider({ url: 'set_auction_as_expired.php' }, callback, data);
        };
        this.registrateBuy = function (callback, data) {
            return _this.routes.getProvider({ url: 'registrate_buy.php' }, callback, data);
        };
        this.getSubscriptionReload = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_subscription_reload.php' }, callback, data);
        };
        this.getSubscription = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_subscription.php' }, callback, data);
        };
        this.getOrderStatus = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_order_status.php' }, callback, data);
        };
        this.cancelOrder = function (callback, data) {
            return _this.routes.getProvider({ url: 'cancel_order.php' }, callback, data);
        };
        this.setGretting = function (callback, data) {
            return _this.routes.getProvider({ url: 'set_gretting.php' }, callback, data);
        };
        this.getOrder = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_order.php' }, callback, data);
        };
        this.getActiveOrder = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_active_order.php' }, callback, data);
        };
        this.getRefferPerUser = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_reffer_per_user.php' }, callback, data);
        };
        this.saveRefferPerUser = function (callback, data) {
            return _this.routes.getProvider({ url: 'save_reffer_per_user.php' }, callback, data);
        };
        this.confirmOrder = function (callback, data) {
            return _this.routes.getProvider({ url: 'confirm_order.php' }, callback, data);
        };
        this.getCommercesWithPrewelcome = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_commerces_with_prewelcome.php' }, callback, data);
        };
        this.savePercentajeShareCommerce = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_save_percentaje_share_menu.php' }, callback, data);
        };
        this.saveShareCommerce = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_save_share_menu.php' }, callback, data);
        };
        this.getForShareMenu = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_commerces_for_share_menu.php' }, callback, data);
        };
        this.setRequestPerProduct = function (callback, data) {
            return _this.routes.getProvider({ url: 'set_request_per_product.php' }, callback, data);
        };
        this.getCarPerUser = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_car_per_user.php' }, callback, data);
        };
        this.saveCarPerUser = function (callback, data) {
            return _this.routes.getProvider({ url: 'save_car_per_user.php' }, callback, data);
        };
        this.getAllvaletParking = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_all_valete_parking.php' }, callback, data);
        };
        this.getCatalogModelCar = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_catalog_model_car.php' }, callback, data);
        };
        this.getCatalogBrandCar = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_catalog_brand_car.php' }, callback, data);
        };
        this.saveUserRegistrationId = function (callback, data) {
            return _this.routes.getProvider({ url: 'save_registration_id.php' }, callback, data);
        };
        this.doCheckIn = function (callback, data) {
            return _this.routes.getProvider({ url: 'do_check_in.php' }, callback, data);
        };
        this.getFaqs = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_faqs.php' }, callback, data);
        };
        this.getWalletReffer = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_wallet_reffer.php' }, callback, data);
        };
        this.getWallet = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_wallet.php' }, callback, data);
        };
        this.getUserCoins = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_user_coins.php' }, callback, data);
        };
        this.getUsersAround = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_users_around.php' }, callback, data);
        };
        this.getUsersAroundForFood = function (callback, data) {
            return _this.routes.getProvider({ url: 'get_users_around_for_food.php' }, callback, data);
        };
        this.getSpecialData = function (callback, data) {
            return _this.routes.getProvider({ url: 'load_special_data.php' }, callback, data);
        };
        this.setDefaults = function (callback) {
            new Promise(function (resolve, reject) {
                if (_this.userObject) {
                    _this.names = (_this.userObject.user_login.names) ? _this.userObject.user_login.names : _this.names;
                    _this.mail = (_this.userObject.user_login.mail) ? _this.userObject.user_login.mail : _this.mail;
                    if (_this.userObject.user_settings != undefined) {
                        _this.kind = (_this.userObject.user_settings.kind) ? _this.userObject.user_settings.kind : _this.kind;
                        _this.birthdate = (_this.userObject.user_settings.birthdate) ? _this.userObject.user_settings.birthdate : _this.birthdate;
                        _this.tracking_time = (_this.userObject.user_settings.tracking_time) ? _this.userObject.user_settings.tracking_time : _this.tracking_time;
                        _this.image = (_this.userObject.user_settings.image) ? _this.userObject.user_settings.image : _this.image;
                        _this.is_able_to_notification = (_this.userObject.user_settings.is_able_to_notification) ? _this.userObject.user_settings.is_able_to_notification : _this.is_able_to_notification;
                        _this.is_able_to_tracking = (_this.userObject.user_settings.is_able_to_tracking) ? _this.userObject.user_settings.is_able_to_tracking : _this.is_able_to_tracking;
                        _this.subscription = (_this.userObject.user_settings.subscription) ? _this.userObject.user_settings.subscription : _this.subscription;
                        _this.commerces_near = (_this.userObject.commerces_near) ? _this.userObject.commerces_near : _this.commerces_near;
                        _this.commerce_status = (_this.userObject.commerce_status) ? _this.userObject.commerce_status : _this.commerce_status;
                        _this.commerce_id = (_this.userObject.commerce_id) ? _this.userObject.commerce_id : _this.commerce_id;
                        _this.short_name = (_this.userObject.user_login.short_name) ? _this.userObject.user_login.short_name : _this.short_name;
                        _this.wallet = (_this.userObject.user_settings.wallet) ? _this.userObject.user_settings.wallet : _this.wallet;
                        _this.reward = (_this.userObject.user_settings.reward) ? _this.userObject.user_settings.reward : _this.reward;
                        if (_this.kind == 3) {
                            _this.range = (_this.userObject.user_settings.range) ? _this.userObject.user_settings.range : _this.range;
                        }
                        if (_this.kind == 1) {
                            _this.names = 'Comercio';
                            _this.short_name = 'Comercio';
                        }
                        else if (_this.kind == 0) {
                            if (_this.platform.is('android') || _this.platform.is('ios')) {
                                if (_this.is_able_to_tracking) {
                                    _this.goTracking();
                                }
                                else {
                                    _this.stopTracking();
                                }
                            }
                        }
                    }
                    _this.enableBackgroundMode();
                    _this.loadPages(function () {
                        if (callback != undefined)
                            callback(true);
                    });
                    resolve();
                }
                reject();
            });
        };
        this.loadPages = function (callback) {
            if (_this.kind == 1) {
                _this.pages = [
                    { title: 'Mi Negocio', icon: 'merchant', component: 'Merchant', logout: false, subscription: 1, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Edita tu Welcome', icon: 'welcome', component: 'Foodhome', logout: false, subscription: 1, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Menú Compartido', icon: 'group', component: 'Share', logout: false, subscription: 1, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Promociones', icon: 'auction', component: 'Specialoffer', logout: false, subscription: 1, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Last Food', icon: 'auction', component: 'Lastfoodoffer', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Subastas en Grupo', icon: 'group', component: 'Auctioncommerce', logout: false, subscription: 1, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Programa de Referidos', icon: 'reffer', component: 'Reffer', logout: false, subscription: 1, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Preguntas Frecuentes', icon: 'help', component: 'Faq', logout: false, subscription: 1, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Menú de App', icon: 'settings', component: 'Account', logout: false, subscription: 1, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Salir', icon: 'log-out', component: 'Home', logout: true, subscription: 1, showBadge: false, badge: 1, additionalData: false, root: true },
                ];
            }
            else if (_this.kind == 3) {
                if (_this.userObject.user_settings.catalog_commerce_user_id == 6 || _this.userObject.user_settings.catalog_commerce_user_id == 1) {
                    _this.pages = [
                        { title: 'Welcome', icon: 'welcome', component: 'Foodhome', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                        { title: 'Preguntas Frecuentes', icon: 'help', component: 'Faq', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                        { title: 'Menú de App', icon: 'settings', component: 'Account', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                        { title: 'Salir', icon: 'log-out', component: 'Home', logout: true, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                    ];
                }
                else if (_this.userObject.user_settings.catalog_commerce_user_id == 9) {
                    _this.pages = [
                        { title: 'Mi Negocio', icon: 'merchant', component: 'Merchant', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                        { title: 'Edita tu Welcome', icon: 'welcome', component: 'Foodhome', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                        { title: 'Preparar Platillos/Bebidas', icon: 'prepare', component: 'Foodprepair', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: { title: 'Preparar Platillos/Bebidas', catalog_commerce_user_id: _this.userObject.user_settings.catalog_commerce_user_id }, root: true },
                        { title: 'Preguntas Frecuentes', icon: 'help', component: 'Faq', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                        { title: 'Menú de App', icon: 'settings', component: 'Account', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                        { title: 'Salir', icon: 'log-out', component: 'Home', logout: true, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                    ];
                }
                else {
                    var title = _this.userObject.user_settings.menu;
                    _this.pages = [
                        { title: title, icon: 'prepare', component: 'Foodprepair', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: { title: title, catalog_commerce_user_id: _this.userObject.user_settings.catalog_commerce_user_id }, root: true },
                        { title: 'Preguntas Frecuentes', icon: 'help', component: 'Faq', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                        { title: 'Menú de App', icon: 'settings', component: 'Account', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                        { title: 'Salir', icon: 'log-out', component: 'Home', logout: true, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                    ];
                }
            }
            else if (_this.kind == 4) {
                if (_this.userObject.user_settings.catalog_commerce_user_id == 7) {
                    _this.pages = [
                        { title: 'Valet Parking', icon: 'valete-parking', component: 'Parking', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                        { title: 'Preguntas Frecuentes', icon: 'help', component: 'Faq', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                        { title: 'Menú de App', icon: 'settings', component: 'Account', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                        { title: 'Salir', icon: 'log-out', component: 'Home', logout: true, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                    ];
                }
                else {
                    _this.pages = [
                        { title: 'Mi Negocio', icon: 'merchant', component: 'Merchant', logout: false, subscription: 1, showBadge: false, badge: 1, additionalData: false, root: true },
                        { title: 'Valet Parking', icon: 'valete-parking', component: 'Parkingusers', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                        { title: 'Preguntas Frecuentes', icon: 'help', component: 'Faq', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                        { title: 'Menú de App', icon: 'settings', component: 'Account', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                        { title: 'Salir', icon: 'log-out', component: 'Home', logout: true, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                    ];
                }
            }
            else if (_this.kind == 0) {
                _this.pages = [
                    { title: 'Welcome', icon: 'welcome', component: 'Foodhomeuser', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Comercios', icon: 'near-map', component: 'StoresHome', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Testing S&W', icon: 'wallet', component: 'SpinandwinPage', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Billetera Electrónica', icon: 'wallet', component: 'Ewallet', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Valet Parking', icon: 'valete-parking', component: 'Parkinguser', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Pre Welcome', icon: 'welcome', component: 'Prewelcome', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Programa de Referidos', icon: 'reffer', component: 'Refferuser', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Subasta tu Consumo en Grupo', icon: 'group', component: 'Auctionuser', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Preguntas Frecuentes', icon: 'help', component: 'Faq', logout: false, subscription: 1, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Menú de App', icon: 'settings', component: 'Account', logout: false, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                    { title: 'Salir', icon: 'log-out', component: 'Home', logout: true, subscription: 0, showBadge: false, badge: 1, additionalData: false, root: true },
                ];
            }
            if (callback != undefined)
                callback();
        };
        this.getUser = function (callback) {
            _this.storage.get("userObject").then(function (data) {
                if (data != null) {
                    _this.userObject = data;
                    _this.logged = true;
                    _this.setDefaults(function (response) {
                        if (callback != undefined)
                            callback(true);
                    });
                }
                else {
                    if (callback != undefined)
                        callback(false);
                }
            });
        };
        this.stopTracking = function () {
            _this.backgroundGeolocation.stop();
        };
        this.enableBackgroundMode = function () {
            var config = {
                silent: true,
            };
            _this.backgroundMode.setDefaults(config);
            _this.backgroundMode.enable();
        };
        this.goTracking = function () {
            _this.checkGPS(function (response) {
                if (response) {
                    _this.startTracking();
                }
            }, { title: 'Activa tu GPS', content: 'Es necesario activar tu localización.' });
        };
        this.checkGPS = function (callback, data) {
            if (_this.platform.is('android') || _this.platform.is('ios')) {
                _this.diagnostic.isLocationEnabled().then(function (isEnabled) {
                    if (!isEnabled) {
                        _this.showRequestForLocalization(data.title, data.content, function (response) {
                            if (response) {
                                _this.diagnostic.switchToLocationSettings();
                                _this.platform.ready().then(function () {
                                    callback(true);
                                });
                            }
                        });
                    }
                    else {
                        _this.platform.ready().then(function () {
                            callback(true);
                        });
                    }
                }).catch(function (errorCallback) {
                });
            }
        };
        this.startTracking = function () {
            var notificationText = _this.commerces_near ? 'Localizamos ' + _this.commerces_near + ' Bares y Restaurantes cerca de ti' : 'Localizando Bares y Restaurantes cerca de ti';
            var config = {
                desiredAccuracy: 0,
                stationaryRadius: 0,
                distanceFilter: 0,
                debug: false,
                notificationTitle: 'Welcome',
                notificationText: notificationText,
                interval: _this.tracking_time,
                stopOnTerminate: true,
                notificationIconLarge: 'ic_stat_gps',
                notificationIconSmall: 'ic_stat_push',
                notificationIconColor: '#00818c',
            };
            _this.backgroundGeolocation.configure(config).subscribe(function (location) {
                // this.Commerce.getCommerceNearOfMeToNotification((response)=>{
                //   console.log('get into background');
                // },{latitude:location.latitude,longitude:location.longitude,mail:this.userObject.user_login.mail,password:this.userObject.user_login.password,status:'3'});
            });
            _this.backgroundGeolocation.start();
        };
        this.getCurrentLocation = function (callback, checkGPS) {
            if (checkGPS || checkGPS == undefined) {
                _this.checkGPS(function (isEnabled) {
                    _this.getCurrentPosition(function (geo_position) {
                        callback(geo_position);
                    });
                }, { title: 'Activa tu GPS', content: 'Es necesario activar tu localización.' });
            }
            else {
                _this.getCurrentPosition(function (geo_position) {
                    callback(geo_position);
                });
            }
        };
        this.getCurrentPosition = function (callback) {
            _this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then(function (geo_position) {
                if (geo_position) {
                    callback(geo_position);
                }
            });
        };
        this.singUpNewUser = function (callback, data) {
            return _this.routes.getProvider({ url: 'singup_user.php' }, callback, data);
        };
        this.saveRegistration = function (welcomepush, callback) {
            if (_this.platform.is('android') || _this.platform.is('ios')) {
                _this.platform.ready().then(function () {
                    _this.oneSignal.startInit(_this.oneSignalProyectId, _this.googleAppId);
                    _this.oneSignal.setSubscription(true);
                    _this.oneSignal.handleNotificationReceived().subscribe(function () {
                    });
                    _this.oneSignal.endInit();
                    _this.oneSignal.getIds().then(function (ids) {
                        _this.registration_id = ids.userId;
                        _this.saveUserRegistrationId(function (response) {
                            callback(response);
                        }, { welcomepush: welcomepush, mail: _this.userObject.user_login.mail, password: _this.userObject.user_login.password, registration_id: ids.userId });
                    });
                });
            }
            else {
                callback({ s: 1, r: "DATA_OK" });
            }
        };
        this.showRequestForLocalization = function (title, message, callback) {
            title = title ? title : 'Activar tú ubicación';
            message = message ? message : '¡Busca negocios cercanos!';
            var confirm = _this.alertCtrl.create({
                title: title,
                message: message,
                buttons: [
                    {
                        text: 'Cancelar',
                        handler: function () {
                            callback(false);
                        }
                    },
                    {
                        text: 'Activar',
                        handler: function () {
                            callback(true);
                        }
                    }
                ]
            });
            confirm.present();
        };
        this.init(function (response) {
            if (response) {
                _this.saveRegistration({}, function (d) {
                });
            }
        });
    }
    User.prototype.init = function (callback) {
        this.OneSignal = false;
        this.oneSignalProyectId = "19099a48-45bc-4026-9a23-3af64c3379ce";
        this.googleAppId = "322869111071";
        this.logged = false;
        this.birthdate = false;
        this.registration_id = 1;
        this.userObject = false;
        this.is_able_to_notification = false;
        this.is_able_to_tracking = false;
        this.subscription = false;
        this.short_name = 'Usuario';
        this.commerces_near = false;
        this.range = false;
        this.gold_coin = false;
        this.reward = 0;
        this.wallet = 0;
        this.kind = false;
        this.silver_coin = false;
        this.image = 'assets/img/upload_photo.png';
        this.names = 'Usuario';
        this.mail = false;
        this.responseOneSignal = false;
        if (callback)
            callback(true);
    };
    User.prototype.getCredentials = function (data) {
        var _this = this;
        var defaults = false;
        new Promise(function (resolve, reject) {
            defaults = {
                mail: _this.userObject.user_login.mail,
                password: _this.userObject.user_login.password,
                company_id: _this.userObject.user_login.company_id
            };
            if (data)
                Object.assign(defaults, data);
            resolve();
        });
        return defaults;
    };
    User.prototype.logout = function (callback) {
        var _this = this;
        this.subscription = false;
        this.commerce_id = false;
        this.kind = false;
        this.image = 'assets/img/upload_photo.png';
        this.names = 'Usuario';
        this.storage.remove('userObject').then(function () {
            _this.storage.remove('prewelcome').then(function () {
                _this.storage.remove('dish_order_per_user').then(function () {
                    _this.storage.remove('order_per_user_id').then(function () {
                        if (callback())
                            callback(true);
                    });
                });
            });
        });
    };
    User.prototype.iniPush = function () {
        var _this = this;
        this.platform.ready().then(function () {
            if (_this.platform.is('android') || _this.platform.is('ios')) {
                _this.oneSignal.startInit(_this.oneSignalProyectId, _this.googleAppId);
                _this.oneSignal.inFocusDisplaying(_this.OneSignal.OSInFocusDisplayOption.InAppAlert);
                _this.oneSignal.setSubscription(true);
                _this.oneSignal.handleNotificationReceived().subscribe(function () {
                    // do something when the notification is received.
                });
                // this.oneSignal.endInit();
            }
        });
    };
    User = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [LoadingController, BackgroundMode, Geolocation, PhotoViewer, BackgroundGeolocation, Storage, Platform, AlertController, Diagnostic, OneSignal, Http, Routes])
    ], User);
    return User;
}());
export { User };
//# sourceMappingURL=user.js.map